package com.kata_3_1_5.entity;

import lombok.*;

@Data
@AllArgsConstructor
@Getter
@Setter
@ToString
public class User {

    private Long id;
    private String name;
    private String lastName;
    private Byte age;

}
