package com.kata_3_1_5;

import com.kata_3_1_5.entity.User;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

public class Users {

    static RestTemplate restTemplate = new RestTemplate();
    static final String URL = "http://91.241.64.178:7081/api/users/";
    static String cookies;
    static User user;


    public static void main(String[] args) {
        getAllUser();
    }

    public static void getAllUser() {
        HttpHeaders headers = new HttpHeaders();
        System.out.println("Создали пустой HttpHeaders " + headers);

        //Тут указываем принимать Json
        headers.setContentType(MediaType.APPLICATION_JSON);
        System.out.println("Поместили туда Content-Type: (принимать тип) типа Json " + headers);

        //Оборачиваю запрос в HttpEntity типа User
        HttpEntity<User> requestUser = new HttpEntity<>(headers);
        System.out.println("внутри обертки для запроса команда на получение в ответе Json " + requestUser);

        //Создали запрос и тут же получает ответ
        ResponseEntity<String> responseUser = restTemplate.exchange(URL, HttpMethod.GET, requestUser, String.class);

        // Плучаю Боди ответа
        System.out.println("Боди ответа " + responseUser.getBody());

        //Выдергиваю Cookie из Хидера
        HttpHeaders headerResponse = responseUser.getHeaders();
        System.out.println("Сам Хидер " + headerResponse);
        cookies = headerResponse.toString().substring(13, 74);
        System.out.println("Сами пиченьки " + cookies);

        //Теперь нужно положить печеньги в последующие хидер запосы
        headers.add("Cookie", cookies);
        System.out.println("Так теперь выглядит наш Хидер " + headers);

        System.out.println("-----------------------------------------------------------------------------");

        //Создаем нового Юзера
        user = new User(3L, "James", "Brown", (byte) 36);
        System.out.println("Проверяем Хидер перед использованием " + headers);
        //Создаем новую обертку для запроса, в Хедере уже есть формат получения Json и Кукиш, плюс передаем Юзера
        requestUser = new HttpEntity<>(user, headers);
        // Добавляем нового юзера
        addUser(requestUser);
        System.out.println("Наш Юзер " + user);

        System.out.println("-----------------------------------------------------------------------------");

        //Меняю Юзеру Имя и Фамилию
        user.setName("Thomas");
        user.setLastName("Shelby");
        System.out.println("Проверяем Хидер перед использованием " + headers);
        //Создаю новую обертку для запроса
        requestUser = new HttpEntity<>(user, headers);
        //Меняю Юзера
        putUser(requestUser);
        System.out.println("Наш Юзер после put " + user);

        System.out.println("-----------------------------------------------------------------------------");

        System.out.println("Проверяем Хидер перед использованием " + headers);
        requestUser = new HttpEntity<>(user, headers);
        deleteUser(requestUser);
        System.out.println("-----------------------------------------------------------------------------");

    }

    public static void addUser(HttpEntity<User> userHttpEntity) {
        //Создали запрос и тут же получает ответ
        ResponseEntity<String> responseUser = restTemplate
                .exchange(URL, HttpMethod.POST, userHttpEntity, String.class);

        //Выдергиваю Боди
        String body = responseUser.getBody();
        System.out.println("Ключик из боди №1 " + body);
    }

    public static void putUser(HttpEntity<User> userHttpEntity) {
        //Создали запрос и тут же получает ответ
        ResponseEntity<String> responseUser = restTemplate
                .exchange(URL, HttpMethod.PUT, userHttpEntity, String.class);

        //Выдергиваю Боди
        String body = responseUser.getBody();
        System.out.println("Ключик из боди №2 " + body);
    }


    public static void deleteUser(HttpEntity<User> userHttpEntity) {
        //Создали запрос и тут же получает ответ
        ResponseEntity<String> responseUser = restTemplate
                .exchange(URL + user.getId(), HttpMethod.DELETE, userHttpEntity, String.class);

        //Выдергиваю Боди
        String body = responseUser.getBody();
        System.out.println("Ключик из боди №3 " + body);
    }

}
